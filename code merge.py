import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os


plt.style.use('classic')

#Creating our path where we will get the Data and where we will export the Results
inpath = os.path.abspath('./Data')
print(inpath)
outpath = os.path.abspath('./Results')
print(outpath)

#Reading the part1 data
part1 = pd.read_csv(inpath + '/part1.csv')
part12= pd.read_csv(inpath + '/part1.csv')
part13= pd.read_csv(inpath + '/part1.csv')
print(part1)


fig, axes = plt.subplots(1, 3)




part1.hist('FIRST_COURSE',bins=100, ax=axes[0])
part12.hist('SECOND_COURSE',bins=100, ax=axes[1])
part13.hist('THIRD_COURSE',bins=100, ax=axes[2])
plt.show()