import pandas as pd
import numpy as np
import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('classic')


df= pd.read_csv('/Users/isobelcooke/PycharmProjects/examrestaurantic/Data/part1.csv')
print(df)

x1=df['FIRST_COURSE']
x2=df['SECOND_COURSE']
x3= df['THIRD_COURSE']

sns.distplot(x1, hist=True,
             color = 'blue', label = 'First course')
sns.distplot(x2, hist=True,
             color = 'red', label = 'Second course')
sns.distplot(x3, hist=True,
             color = 'yellow', label = 'Third Course')
plt.show()
